## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



## Exclude linux-ppc64e6500
##EXCLUDE_ARCHS = linux-ppc64e6500

APP:=MCoreUtilsApp
APPSRC:=$(APP)


USR_INCLUDES += -I$(where_am_I)$(APPSRC)

USR_CFLAGS += -D_GNU_SOURCE
USR_CFLAGS += -DVERSION=\"$(LIBVERSION)\"

SOURCES += $(APPSRC)/threadShow.c
SOURCES += $(APPSRC)/threadRules.c
SOURCES += $(APPSRC)/memLock.c
SOURCES += $(APPSRC)/shellCommands.c
SOURCES += $(APPSRC)/utils.c


DBDS += $(APPSRC)/mcoreutils.dbd


.PHONY: 
.PHONY: vlibs
vlibs:
